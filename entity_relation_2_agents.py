import sys
import os.path
import re
import uuid
import json
import requests
import time
import random
from random import randint
import csv
import itertools
from string import digits


import apiai
from gtts import gTTS
import speech_recognition as sr
import pygame
from pygame import mixer


def create_user():
    sessionId = re.sub('[^A-Za-z0-9]+', '', str(uuid.uuid1()))
    # print ("\nSession ID : ", sessionId)
    return sessionId


################################### GET INTENT NAME FROM APIAI #######################
def get_response_dict_apiai(user_says, ai):
    # getting reponse from api.ai in json format
    request = ai.text_request()
    request.session_id = sessionId
    request.query = user_says
    response = request.getresponse()
    response_dict = json.loads(response.read().decode('utf-8'))
    # print ("response_dict : ", response_dict)
    return response_dict
######################################################################################


################################# ENTITY APIs ##########################################
def get_entities(headers) :
    r = requests.get("https://api.api.ai/v1/entities?v=20150910", headers=headers)
    return json.loads(r.text)

def add_entities(headers, entities2add) :
    r = requests.put("https://api.api.ai/v1/entities?v=20150910", data = json.dumps(entities2add), headers = headers)

def add_entries(headers, entity_id, entries2add) :
    r = requests.put("https://api.api.ai/v1/entities/%s/entries?v=20150910"% entity_id, data = json.dumps(entries2add), headers = headers)

def delete_entity(headers, entity_id) :
    r = requests.delete("https://api.api.ai/v1/entities/%s?v=20150910"% entity_id, headers = headers)

def delete_entries(headers, entity_id, entries2delete) :
    r = requests.delete("https://api.api.ai/v1/entities/%s/entries?v=20150910"% entity_id, data = json.dumps(entries2delete), headers = headers)
########################################################################################


######################### CONTEXT APIs ###########################
def get_active_contexts_apiai(headers):
    r = requests.get("https://api.api.ai/v1/contexts?sessionId=%s"% sessionId, headers = headers)

    return json.loads(r.text)

def get_specific_context_apiai(headers, context):
    r = requests.get("https://api.api.ai/v1/contexts/%s?sessionId=%s"% context% sessionId, headers = headers)
    for active_context in r:
        return active_context

def add_contexts_apiai(headers, context):
    r = requests.post("https://api.api.ai/v1/contexts?sessionId=%s"% sessionId, data = json.dumps(context), headers = headers)

def delete_context_apiai(headers, context):
    r = requests.delete("https://api.api.ai/v1/contexts/%s?sessionId=%s"% context% sessionId, headers = headers)

def delete_all_contexts_apiai(headers):
    r = requests.delete("https://api.api.ai/v1/contexts?sessionId=%s"% sessionId, headers = headers)
#################################################################################


##################### INTENT APIs ###################################################
def get_intent(headers) :
    r = requests.get("https://api.api.ai/v1/intents?v=20150910", headers = headers)

    return json.loads(r.text)


def create_intent(headers, intent2add) :
    r = requests.post("https://api.api.ai/v1/intents?v=20150910", data=json.dumps(intent2add), headers = headers)

    return json.loads(r.text)

def update_intent(headers, intent_id, intent2update) :
    r = requests.put("https://api.api.ai/v1/intents/%s?v=20150910"% intent_id, data=json.dumps(intent2update), headers = headers)

    return json.loads(r.text)

def delete_intent(headers, intent_id) :
    r = requests.put("https://api.api.ai/v1/intents/%s?v=20150910"% intent_id, headers = headers)
#####################################################################################


def get_new_items(path, headers) :

    with open(path, "r") as f :
        reader = csv.reader(f, delimiter=",")
        new_items = []
        update_items = []

        intents = get_intent(headers)
        # print ("\nintents : ", intents)

        current_intent_names = []
        current_intent_ids = []
        for intent in intents :
            current_intent_names.append(intent["name"])
            current_intent_ids.append(intent["id"])

        for row in reader :
            if row[0] == "name" :
                continue

            if row[0] :
                try :
                    if already_exist == False :
                        new_items.append(new_item)
                    else :
                        update_items.append(new_item)
                except :
                    pass
                already_exist = False
                for i in range(len(current_intent_names)) :
                    if row[0].strip().lower() in current_intent_names[i].lower() :
                        new_item = {"id": "", "name":"", "qualifiers":{}}
                        new_item["name"] = row[0].strip().lower()
                        new_item["id"] = current_intent_ids[i]
                        already_exist = True
                
                if already_exist == False :
                    new_item = {"name":"", "qualifiers":{}}
                    new_item["name"] = row[0].strip().lower()
            #     else :
            #         continue
            # elif (already_exist == True) :
            #     continue
                    
            qualifier = row[2].strip()
            new_item["qualifiers"][qualifier] = {}
            
            entries = row[3].strip()
            entries = entries.replace(" ", "").split(",")
            new_item["qualifiers"][qualifier]["entries"] = entries

            required = row[4].strip().lower()
            if required == "true" :
                new_item["qualifiers"][qualifier]["required"] = True
            else :
                new_item["qualifiers"][qualifier]["required"] = False

            isList = row[5].strip().lower()
            if isList == "true" :
                new_item["qualifiers"][qualifier]["isList"] = True
            else :
                new_item["qualifiers"][qualifier]["isList"] = False

        if already_exist == False :
            new_items.append(new_item)
        else :
            update_items.append(new_item)


    # print ("\nnew_items :\n", new_items)
    # print ("\nupdate_items : \n", update_items)

    return new_items, update_items 


def get_training_templates(path) :

    with open(path, "r") as f :
        reader = csv.reader(f, delimiter=",")
        training_templates = []
        for row in reader :
            training_templates.append(row[0].strip().lower())

    return training_templates


def get_follow_up_templates(path) :

    with open(path, "r") as f :
        reader = csv.reader(f, delimiter=",")
        follow_up_templates = []
        for row in reader :
            follow_up_templates.append(row[0].strip().lower())

    return follow_up_templates


def add_new_entity(entity_name, entries) :

    entity2add = {
                  "entries": [],
                  "name": entity_name
                }

    for entry in entries :
        entry_json = {"value" : entry}
        entity2add["entries"].append(entry_json)

    add_entities(headers, entity2add)


def get_parameters_templates_json(items2add, items_comb, items_json, training_templates) :

    parameters_json = []
    
    templates_json = []

    entries = []
    entity_names = []

    sys_number_template = "@sys.number:number"

    for item in items2add :

        item_name = item["name"]
        qualifiers = item["qualifiers"]

        for q in qualifiers.keys() :

            param_dict = {}

            entries = qualifiers[q]["entries"]
            entity_name = item_name + "-" + q
            entity_names.append(entity_name)
            # print ("\nentity_name : ", entity_name)
            # print ("entity : ", q)
            # print ("entries : ", entries)

            param_dict["dataType"] = "@" + entity_name
            param_dict["isList"] = qualifiers[q]["isList"]
            param_dict["name"] = entity_name
            prompts = []
            param_dict["prompts"] = prompts
            param_dict["required"] = qualifiers[q]["required"]
            param_dict["value"] = "$" + entity_name

            # print ("\nparam_dict : ", param_dict)

            parameters_json.append(param_dict)


    for items in items_comb :

        for train_temp in training_templates :

            train_template = train_temp

            count = 1
            while (count <= 2) :

                q1 = None
                q2 = None
                templates = []
                if items[0] != 'None' :
                    q1 = list(random.choice(items_json[items[0]]))

                    # print ("\nq1: ", q1)

                    temp = ""
                    temp_item1 = ""
                    for i in range(len(q1)) :
                        if q1[i] != 'None' :
                            # temp = "@" + items[0] + "-" + q1[i] + ":" + items[0] + "-" + q1[i]
                            temp += ("@" + items[0] + "-" + q1[i] + ":" + items[0] + "-" + q1[i]) + " "

                    temp_item1 = temp + items[0]

                    templates.append(temp_item1)

                if items[1] != 'None' :
                    q2 = list(random.choice(items_json[items[1]]))

                    # print ("q2: ", q2)

                    temp = ""
                    temp_item2 = ""
                    for i in range(len(q2)) :
                        if q2[i] != 'None' :
                            # temp = "@" + items[1] + "-" + q2[i] + ":" + items[1] + "-" + q2[i]
                            temp += ("@" + items[1] + "-" + q2[i] + ":" + items[1] + "-" + q2[i]) + " "

                    temp_item2 = temp  + items[1]

                    templates.append(temp_item2)


                # print ("templates: ", templates)

                t = ""
                for i in range(len(templates)) :

                    sys_number_choice = random.choice([1])

                    if (i == len(templates)-1 and i != 0) :
                        if sys_number_choice == 1 :
                            t += "and " + sys_number_template+"-"+str(len(templates)) + " " + templates[i]
                        else :
                            t += "and " + templates[i]

                    elif (i == len(templates)-1) :
                        if sys_number_choice == 1 :
                            t += sys_number_template+"-"+str(len(templates)) + " " + templates[i]
                        else :
                            t += templates[i]
                    else :
                        if sys_number_choice == 1 :
                            t += sys_number_template+"-"+str(i+1) + " " + templates[i] + " "
                        else :
                            t += templates[i] + " "
                    # print ("\ntemplate: ", template)

                train_template = train_temp.replace("<quantities> <qualifiers> <items>", t)

                # print ("train_template: ", train_template)
                templates_json.append(train_template)


                count += 1


    # print ("\nparameters_json : ", parameters_json)
    # print ("\ntemplate_json : ", templates_json)


    # print ("\ntemplate_json: ", templates_json)

    # print ("\ntraining_templates : ", training_templates)

    return parameters_json, templates_json


def add_new_item_conversation(items2add, items_comb, items_json, training_templates) :

    parameters_json, templates_json = get_parameters_templates_json(items2add, items_comb, items_json, training_templates)

    print ("\nparameters_json: ", parameters_json)

    print ("\ntemplates_json: ", templates_json[0])

    print ("\nitems2add: ", items2add)


    user_says_json = []
    user_says_json2 = []
    
    for t in templates_json :

        t2words = t.split()

        userSays_temp = {}
        userSays_temp["count"] = 0
        userSays_temp["data"] = []

        userSays_temp2 = {}
        userSays_temp2["count"] = 0
        userSays_temp2["data"] = []

        text = ""

        for i in range(len(t2words)) :
            text_json = {}

            if "@" in t2words[i] :

                if (i!=0) :
                    text_json = {}
                    if text:
                        text_json["text"] = text.replace("  ", " ")
                        userSays_temp["data"].append(text_json)
                        userSays_temp2["data"].append(text_json)
                        text = ""

                if "number" in t2words[i] :

                    text_json = {}
                    text_json["text"] = " "
                    userSays_temp["data"].append(text_json)

                    text_json2 = {}
                    a = t2words[i].split(":")
                    text_json2["alias"] = a[1]
                    text_json2["meta"] = a[0]
                    text_json2["text"] = randint(1, 20)
                    text_json2["userDefined"] = True
                    userSays_temp2["data"].append(text_json2)

                    # text_json = {}
                    # text_json["text"] = text_json2["text"]
                    # userSays_temp["data"].append(text_json)

                for item in items2add :

                    if item["name"] in t2words[i] :

                        for q in item["qualifiers"].keys() :

                            if q in t2words[i] :

                                text_json = {}
                                text_json["text"] = " "
                                userSays_temp["data"].append(text_json)
                                userSays_temp2["data"].append(text_json)      

                                text_json = {}
                                a = t2words[i].split(":")
                                text_json["alias"] = a[1]
                                text_json["meta"] = a[0]
                                text_json["text"] = random.choice(item["qualifiers"][q]["entries"])
                                text_json["userDefined"] = True
                                userSays_temp["data"].append(text_json)

                                text_json2 = {}
                                text_json2["text"] = text_json["text"]
                                userSays_temp2["data"].append(text_json2)

            else :
                text += (" " + t2words[i] + " ")

            if (i == len(t2words)-1) :
                
                text_json = {}
                text_json["text"] = text.replace("  ", " ")
                userSays_temp["data"].append(text_json)
                userSays_temp2["data"].append(text_json)   


        user_says_json.append(userSays_temp)
        user_says_json2.append(userSays_temp2)

        # print ("\ntemplate: ", t)
        # print ("\nuserSays_temp: ", userSays_temp)


    random.shuffle(user_says_json)
    random.shuffle(user_says_json2)
    ######## Prepare Intent Json ####################################
    intent_json = {
                      "contexts": [],
                      "events": [],
                      "fallbackIntent": False,
                      "name": "get orders examples",
                      "priority": 500000,
                      "responses": [
                        {
                          "action": "add.list",
                          "affectedContexts": [
                            {
                              "lifespan": 5,
                              "name": "order_placed",
                              "parameters": {}
                            }
                          ],
                          "defaultResponsePlatforms": {
                            "google": True
                          },
                          "messages": [
                            {
                              "platform": "google",
                              "textToSpeech": "Okay. Your order will be prepared shortly. Thanks!",
                              "type": "simple_response"
                            },
                            {
                              "speech": "Okay. Your order will be prepared shortly. Thanks!",
                              "type": 0
                            }
                          ],
                          "parameters": [],
                          "resetContexts": False
                        }
                      ],
                      # "templates": templates_json,
                      "userSays": [],
                      "webhookForSlotFilling": False,
                      "webhookUsed": False
                    }


    # print ("\nuser_says_json: ", user_says_json[:5])
    # print ("\nuser_says_json2: ", user_says_json2[:5])

    
    # Agent 1
    intent_json1 = intent_json
    intent_json1["name"] = "get orders - items"
    intent_json1["responses"][0]["parameters"] = parameters_json
    intent_json1["userSays"] = user_says_json

    # print("\nintent_json1: ", user_says_json)

    create_intent(headers, intent_json1)

    # Agent 2
    intent_json2 = intent_json
    intent_json2["name"] = "get orders - quantities"
    intent_json2["responses"][0]["parameters"] = []
    intent_json2["userSays"] = user_says_json2

    # print("\nintent_json2: ", user_says_json2)

    create_intent(headers2, intent_json2)




if __name__ == "__main__" :

    #############################################################################
    DEVELOPER_ACCESS_TOKEN_agent1 = "783b35ab2d3347eea407c1e346b1c1fa"

    ai_agent1 = apiai.ApiAI(DEVELOPER_ACCESS_TOKEN_agent1)

    headers = {"Accept" : "application/json", "Content-Type" : "application/json", "Authorization" : "Bearer %s"% DEVELOPER_ACCESS_TOKEN_agent1}
    ###############################################################################


    #############################################################################
    DEVELOPER_ACCESS_TOKEN_agent2 = "95774268fe3043428bb9ccd8cfd3655b"

    ai_agent2 = apiai.ApiAI(DEVELOPER_ACCESS_TOKEN_agent2)

    headers2 = {"Accept" : "application/json", "Content-Type" : "application/json", "Authorization" : "Bearer %s"% DEVELOPER_ACCESS_TOKEN_agent2}
    ###############################################################################





    training_templates = get_training_templates("training_text_templates.csv")
    # print ("\ntraining templates -->\n", training_templates)

    follow_up_templates = get_follow_up_templates("follow_up_templates.csv")
    # print ("\nfollow_up_templates -->\n", follow_up_templates)

    items2add, items2update = get_new_items("menu_items.csv", headers)

    print ("\nitems2add: ", items2add)
    print ("\nitems2update: ", items2update)


    items = []
    qualifiers = []
    items_json = {}

    for item in items2add :
        items.append(item['name'])
        items_json[item['name']] = list(itertools.combinations(tuple(item['qualifiers'])+tuple(('None',)),2))

        # items_json[item['name']] = list(itertools.combinations(tuple(item['qualifiers']),2))

        
        item_name = item["name"]
        qualifiers = item["qualifiers"]

        for q in qualifiers.keys() :

            param_dict = {}

            entries = qualifiers[q]["entries"]
            entity_name = item_name + "-" + q
            add_new_entity(entity_name, entries)

    if items2add :
        print ("\nAdded new items ...")

    items.append('None')
    items_comb = list(itertools.combinations(items, 2))
   
    print ("\nitems_comb: ", items_comb)
    print ("\nitems_json: ", items_json)



    sys_numbers_comb = [('1', '2'), ('1', 'None'), ('None', '2')]

    print ("\nsys_numbers_comb: ", sys_numbers_comb)

    add_new_item_conversation(items2add, items_comb, items_json, training_templates)

    print ("\nCreated new intent ...")

    having_conversation = True

    while (having_conversation == True) :


        sessionId = create_user()
        # print ("\nsessionId ---> ", sessionId)

        end_chat = False

        while (end_chat != True) :

            user_says = input("\nUser says: ")

            if not user_says :
                continue


            user_says_agent1 = user_says.translate({ord(k): None for k in digits})

            response_dict_agent1 = get_response_dict_apiai(user_says_agent1, ai_agent1)

            response_dict_agent2 = get_response_dict_apiai(user_says, ai_agent2)
            # bot_says = response_dict["result"]["fulfillment"]["speech"]

            # print ("Bot says: ", bot_says)

            print ("\nSlots_agent1: ", response_dict_agent1["result"]["parameters"])
            print ("\nSlots_agent2: ", response_dict_agent2["result"]["parameters"])


          