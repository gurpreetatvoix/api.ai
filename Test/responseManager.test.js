var ResponseManager = require('./../Managers/ResponseManager.js');

//console.log(ResponseManager.getResponse('orderPrompts', 'salad', 'TESTONLY'));

    test('check pizza orderprompt', () => {
    expect(ResponseManager.getResponse('orderPrompts', 'pizza', 'TESTONLY')).toBe('This is for test pizza only. DO NOT MODIFY');
    
});

test('check salad orderprompt', () => {
    expect(ResponseManager.getResponse('orderPrompts', 'salad', 'TESTONLY')).toBe('This is for test salad only. DO NOT MODIFY');
    
});
test('check positive sentiment', () => {
    expect(ResponseManager.getResponse('positiveSentiment', '', 'TESTONLY')).toBe('This is for test positive sentiment only. DO NOT MODIFY');
    
});

